import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }
  //crea un observable que mantiene el estado de una variable de sesion
  private loggedIn = new BehaviorSubject<boolean>(this.tokenAvailable()); // {1}

  get isLoggedIn() {
    return this.loggedIn.asObservable(); // {2}
  }
  //metodo para cerrar sesion, elimina la variable de sesion donde se almacena el estado de la sesion
  logout(): void {
    sessionStorage.removeItem('isLoggedIn');
    //asigna al observable un estado false, que significa que la sesion finalizo
    this.loggedIn.next(false);
  }
  getToken() {

    return sessionStorage.getItem('isLoggedIn');
  }

  private tokenAvailable(): boolean {
    return sessionStorage.getItem('isLoggedIn') !== null;
  }
  //Metodo de inicio de sesion, crea una variable de sesion y la asigna al observable
  login(token: string): void {
    sessionStorage.setItem('isLoggedIn', token);
    this.loggedIn.next(true);
  }

}
