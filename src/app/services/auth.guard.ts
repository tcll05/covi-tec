import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private auth: AuthService) { }
  ///metodo heredado, verifica la url, si necesita ser "vigilada" para validar la autenticacion 
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    let url: string = state.url;
    return this.verifyLogin(url);
  }
  // al ejecutar una accion en las routes, verifica si la variable de sesion es verdadera, 
  //si es falsa redirecciona al login
  verifyLogin(url): boolean {
    if (!this.isLoggedIn()) {
      this.router.navigate(['/']);
      return false;
    }
    else if (this.isLoggedIn()) {
      return true;
    }
  }
  //metodo que verifica la variable de sesion
  public isLoggedIn(): boolean {
    let status = false;
    if (sessionStorage.getItem('isLoggedIn') !== null) {
      status = true;
    }
    else {
      status = false;
    }
    return status;
  }

  
}
