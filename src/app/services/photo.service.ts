import { Injectable } from '@angular/core';
import {
  Plugins, CameraResultType, Capacitor, FilesystemDirectory,
  CameraPhoto, CameraSource
} from '@capacitor/core';

const { Camera, Filesystem, Storage } = Plugins;

@Injectable({
  providedIn: 'root'
})
export class PhotoService {
  constructor() { }
  
  public async addNewToGallery() {
    const capturedPhoto = await Camera.getPhoto({
      resultType: CameraResultType.Base64,
      source: CameraSource.Camera,
      width: 320,
      height: 480,
      quality: 90
    })
      .then((photo) => {
        //this.photo = photo.base64String;
        localStorage.setItem('covi_img', photo.base64String);
      }, (err) => {
        console.log(err);
      });
    //return this.photo;
  }
  
}
