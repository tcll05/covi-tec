import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RequestService {

  headers = new HttpHeaders().set('Content-Type', 'application/json');
  
  constructor(private http: HttpClient) { }

  getAll<T>(url: string): Observable<T> {
    return this.http.get<T>(`${environment.baseUrl}/${url}`, { headers: this.headers });
  }

  getById<T>(url: string, id: number): Observable<T> {
    return this.http.get<T>(`${environment.baseUrl}/${url}/${id}`, { headers: this.headers });
  }

  post<T>(url: string, data): Observable<T> {
    return this.http.post<T>(`${environment.baseUrl}/${url}`, data, { headers: this.headers });
  }

  postLogin<T>(url: string, data): Observable<T> {
    return this.http.post<T>(`${environment.baseUrl}/${url}`, data);
  }

  put<bool>(url: string, data, id: number): Observable<bool> {
    return this.http.put<bool>(`${environment.baseUrl}/${url}/${id}`, data, { headers: this.headers });
  }
  delete<bool>(url: string, id: number): Observable<bool> {
    return this.http.delete<bool>(`${environment.baseUrl}/${url}/${id}`, { headers: this.headers });
  }

}
