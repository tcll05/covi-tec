import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RequestService } from 'src/app/services/request.service';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { AlertController, LoadingController, ToastController } from '@ionic/angular';
import { LoginResponse } from 'src/app/models/LoginResponse';
//import { LocalNotifications } from '@ionic-native/local-notifications/ngx';

import { Plugins } from '@capacitor/core';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

    formLogin: FormGroup;

    constructor(
        public alertController: AlertController,
        public loadingCtrl: LoadingController,
        private builder: FormBuilder,
        private request: RequestService,
        private sesion: AuthService,
        private router: Router
    ) { }

    ngOnInit() {
        this.formLogin = this.builder.group({
            nombre: ['', [Validators.required]],
            contrasenia: ['', [Validators.required]],
        });
    }

    cargarCatalogos(operadorId: number) {
        this.request.getAll<any>(`tec/catalogos/${operadorId}`).subscribe(
            result => {
                localStorage.setItem("catalogos", JSON.stringify(result));
            }
        )
    }

    async submit() {
        if (this.formLogin.invalid) {
            this.formLogin.markAllAsTouched();
            return;
        }
        else {
            var formData = new FormData();
            formData.append("nombre", this.formLogin.get('nombre').value);
            formData.append("contrasenia", this.formLogin.get('contrasenia').value);

            let loading = await this.loadingCtrl.create({
                message: 'Autenticando...',
            });


            await loading.present();
            this.request.postLogin<LoginResponse>("autenticacion", formData).subscribe(
                result => {
                    loading.dismiss();
                    if (result.resultado == false) {
                        this.presentAlert("Error", "Error al iniciar sesion.");
                        sessionStorage.removeItem("isLoggedIn");
                    }
                    else {
                        if (result.cargoId == 5) {
                            this.sesion.login(result.token);
                            localStorage.setItem("usuario", JSON.stringify(result));
                            this.cargarCatalogos(result.operadorId);
                            this.formLogin.reset();
                            this.router.navigate(['/peaje']);
                        }
                        else{
                            this.presentAlert("Error", "Su cargo no tiene habilitada esta opcion.");
                        }

                    }
                },
                error => {
                    loading.dismiss();
                    this.presentAlert("Error", "Credenciales inválidas.");
                    sessionStorage.removeItem('isLoggedIn');
                }
            );
        }
    }

    async presentAlert(title: string, text: string) {
        const alert = await this.alertController.create({
            header: title,
            message: text,
            buttons: ['Aceptar']
        });
        await alert.present();
    }
}
