import { Component, OnInit, ViewChild } from '@angular/core';
import { isNullOrUndefined } from 'util';
import { TecVehiculo } from 'src/app/models/Catalogos';
import { RequestService } from 'src/app/services/request.service';
import { Tec } from 'src/app/models/Tec';
import { AlertController, LoadingController, IonBackButtonDelegate } from '@ionic/angular';
import { BackgroundTaskService } from 'src/app/services/background-task.service';
import { ForegroundService } from '@ionic-native/foreground-service/ngx';
import { RouteConfigLoadEnd, Router } from '@angular/router';

@Component({
    selector: 'app-salida',
    templateUrl: './salida.component.html',
    styleUrls: ['./salida.component.scss'],
})
export class SalidaComponent implements OnInit {

    constructor(
        private request: RequestService,
        private alertController: AlertController,
        public loadingCtrl: LoadingController,
        private router: Router
    ) { }

    entradas: TecVehiculo[] = [];
    operadorId: number;
    usuarioId: number;
    carrilId: number;
    confirmacion: boolean = true;
    private intervalId: any;
    @ViewChild(IonBackButtonDelegate, { static: false }) backButton: IonBackButtonDelegate;

    ngOnInit() {
        this.carrilId = parseInt(localStorage.getItem("carrilSeleccionado"))
        this.sincronizar();
        //this.foreground.run();
        //this.foreground.iniciarTec(this.entradas);
        this.iniciarTec();
    }

    ionViewDidLeave() {
        clearInterval(this.intervalId);
    }

    async presentAlertConfirm(tec: TecVehiculo) {
        const alert = await this.alertController.create({
            cssClass: 'my-custom-class',
            header: `¿Desea confirmar la salida del vehículo (${tec.placa})?`,
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                        console.log('Confirm Cancel: blah');
                    }
                }, {
                    text: 'Confirmar',
                    handler: () => {
                        this.save(tec);
                    }
                }
            ]
        });

        await alert.present();
    }

    save(tec: TecVehiculo) {
        let tmpTec: Tec = new Tec(tec.id, this.usuarioId, tec.placa, this.carrilId);
        this.request.post<any>(`tec/salida`, tmpTec).subscribe(
            result => {
                if (result.resultado == true) {
                    this.sincronizar();
                }
                else {

                }
            }
        )
    }

    registrarSalida(tec: TecVehiculo) {
        if (this.confirmacion) {
            this.presentAlertConfirm(tec);
        }
        else {
            this.save(tec);
        }

    }

    async sincronizar() {
        let loading = await this.loadingCtrl.create({
            message: 'Cargando...',
        });
        await loading.present();

        let usuario = JSON.parse(localStorage.getItem("usuario"));
        this.operadorId = usuario.operadorId;
        this.usuarioId = usuario.usuarioId;
        this.request.getAll<TecVehiculo[]>(`tec/vehiculos/${this.operadorId}`).subscribe(
            result => {
                loading.dismiss();
                this.entradas = result;
            },
            error => {
                console.log(error);
            }
        )
    }

    buscar(buscarText: string) {
        if (isNullOrUndefined(buscarText) || buscarText == '')
            return this.entradas;

        return this.entradas.filter(entrada =>
            entrada.placa.indexOf(buscarText.toUpperCase()) > -1);
    }

    private iniciarTec() {
        this.intervalId = setInterval(() => {
            this.sincronizar();
        }, 10000);
    }


}
