import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { isNullOrUndefined } from 'util';
import { isEmpty } from 'rxjs/operators';
import { TipoVehiculo, Carril } from 'src/app/models/Catalogos';
import { RequestService } from 'src/app/services/request.service';
import { AlertController, ToastController } from '@ionic/angular';
import { Tec } from 'src/app/models/Tec';

@Component({
	selector: 'app-entrada',
	templateUrl: './entrada.component.html',
	styleUrls: ['./entrada.component.scss'],
})
export class EntradaComponent implements OnInit {
	formEntrada: FormGroup;
	tipoVehiculos: TipoVehiculo[];
	allCarriles: Carril[];
	carril: number = 0;
	carrilId: number;
	constructor(private builder: FormBuilder, private request: RequestService, public toastController: ToastController) { }

	ngOnInit() {
		this.formEntrada = this.builder.group(
			{
				numeroPlaca: [null, [Validators.required, Validators.pattern("^[A-Za-z0-9]*$"), Validators.maxLength(10), Validators.minLength(5)]],
				idTipoVehiculo: [0],
				numeroEjes: [null, [Validators.pattern("^[0-9]*$"), Validators.maxLength(2)]],
			}
		);
		this.getCatalogos();
		this.carrilId = parseInt(localStorage.getItem("carrilSeleccionado"));
		this.carril = this.allCarriles.find(x => x.id == this.carrilId).numero;

	}

	getCatalogos() {
		this.tipoVehiculos = JSON.parse(localStorage.getItem("catalogos")).vehiculosTipo;
		this.allCarriles = JSON.parse(localStorage.getItem("catalogos")).carriles;
	}

	guardar() {
		if (isNullOrUndefined(this.formEntrada.get("numeroEjes").value) || this.formEntrada.get("numeroEjes").value == "") {
			this.formEntrada.patchValue({ "numeroEjes": 0 })
		}
		else
		if (this.formEntrada.get("numeroEjes").value == 0) {
			this.presentToast("Digite un número de ejes mayor a Cero")
			return;
		}
		
		if (this.formEntrada.invalid) {
			this.formEntrada.markAllAsTouched();
			return;
		}
		else {
			let save: Tec = this.formEntrada.value;
			save.numeroEjes = +save.numeroEjes;
			let usuario = JSON.parse(localStorage.getItem("usuario"));
			save.usuarioId = usuario.usuarioId;
			save.carrilId = this.carrilId;
			this.request.post<any>("tec/entrada", save).subscribe(
				result => {
					if (result.resultado == true) {
						this.formEntrada.reset();
						this.presentToast("Vehículo agregado a la cola")
					}
					else {

					}
				}
			)

		}
	}

	async presentToast(mensaje:string) {
		const toast = await this.toastController.create({
		  message: mensaje,
		  duration: 2000
		});
		toast.present();
	  }
}
