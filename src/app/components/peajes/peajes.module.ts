import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PeajesRoutingModule } from './peajes-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IndexPeajesComponent } from './index-peajes/index-peajes.component';
import { EntradaComponent } from './entrada/entrada.component';
import { SalidaComponent } from './salida/salida.component';
import { IonicModule } from '@ionic/angular';


@NgModule({
  declarations: [IndexPeajesComponent, EntradaComponent, SalidaComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    PeajesRoutingModule,
    IonicModule,
  ],
})
export class PeajesModule { }
