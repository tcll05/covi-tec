import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Carril } from 'src/app/models/Catalogos';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-index-peajes',
  templateUrl: './index-peajes.component.html',
  styleUrls: ['./index-peajes.component.scss'],
})
export class IndexPeajesComponent implements OnInit {
  carriles: Carril[];
  constructor(
    private sesion: AuthService,
    private router: Router,
    public alertController: AlertController
  ) { }

  ngOnInit() {

  }

  setInputs(tipo: number) {
    let allCarriles: Carril[] = JSON.parse(localStorage.getItem("catalogos")).carriles;
    //let 
    this.carriles = JSON.parse(localStorage.getItem("catalogos")).carriles;//allCarriles.filter(x => x.tipo == tipo);
  }

  async presentAlertPrompt(tipo: number) {
    this.setInputs(tipo);
    let input: any = { data: [] };
    let i = 0;
    this.carriles.forEach(element => {
      i++;
      input.data.push({ name: "radio" + i, type: 'radio', label: `Carril no. ${element.numero}`, value: `${element.id}` })
      
    });
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Seleccionar carril',
      inputs:
        input.data
      ,
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        }, {
          text: 'Aceptar',
          handler: (data: string) => {
            if (isNullOrUndefined(data)) {
              return;
            }
            else {
              localStorage.setItem("carrilSeleccionado", data);
              if (tipo == 1)
                this.router.navigate(["/peaje/entrada"])
              else
                this.router.navigate(["/peaje/salida"])
              console.log('Confirm Ok');
            }

          }
        }
      ]
    });

    await alert.present();
  }

  registroEntada() {

    this.presentAlertPrompt(1);
  }
  registroSalida() {
    this.presentAlertPrompt(2);
  }

  cerrarSesion() {
    this.sesion.logout();
    this.router.navigateByUrl("/")
  }
}