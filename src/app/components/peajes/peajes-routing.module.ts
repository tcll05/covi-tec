import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexPeajesComponent } from './index-peajes/index-peajes.component';
import { EntradaComponent } from './entrada/entrada.component';
import { SalidaComponent } from './salida/salida.component';


const routes: Routes = [
  {
    component: IndexPeajesComponent,
    path: ''
  },
  {
    component: EntradaComponent,
    path: 'entrada'
  },
  {
    component: SalidaComponent,
    path: 'salida'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PeajesRoutingModule { }
