export class Tec {
  tecId: number;
  usuarioId: number;
  numeroPlaca: string;
  carrilId: number;
  idTipoVehiculo: number;
  numeroEjes: number;
  
  constructor($tecId: number, $UsuarioId: number, $NumeroPlaca: string, $CarrilId: number) {
    this.usuarioId = $UsuarioId;
    this.numeroPlaca = $NumeroPlaca;
    this.carrilId = $CarrilId;
    this.tecId = $tecId;
  }
}
