export class LoginResponse {
    resultado:boolean;
    token:string;
    expiraEn:number;
    usuarioId:number;
    operadorId:number;
    rolId:number;
    cargoId: number; // si es == 5 pasa, sino, su cargo no tiene habilitada esta opcion
}

export class LoginPost {
    nombre:string;
    contrasenia:string;
}