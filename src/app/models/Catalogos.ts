export class TipoVehiculo {
    idTipoVehiculo: number;
    tipoVehiculo1: string;

    constructor(IdTipoVehiculo: number, TipoVehiculo: string) {
        this.idTipoVehiculo = IdTipoVehiculo;
        this.tipoVehiculo1 = TipoVehiculo;
    }
}

export class Carril {
    id: number;
    numero: number;
    tipo: number;
}

export class TecVehiculo {
    id: number
    placa: string;
    fecha: string;
    carrilEntrada: number
}
